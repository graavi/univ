Antropologi mempelajari manusia sebagai makhluk biologis sekaligus makhluk sosial.

Kampus bisa menjadi tempat bagi mahasiswa untuk mengembangkan aktualisasi dan apresiasinya sesuai dengan kebutuhannya.

Secara garis besar antropologi itu ilmu yang mempelajari manusia dan kampus itu adalah tempat berkumpulnya pada manusia-manusia,, yahh dengan tujuan yang mungkin berbeda, sama.

Pada dasarnya,, antropologi kampus berusaha menjelaskan bagaiman kehidupan mahasiswa dalam lingkungan kampus, terkhusus mahasiswa sebagai pemeran utama. Dimana mahasiswa tersebut melakukan pertukaran informasi dengan dosen ataupun sesama mahasiswa, melakukan kegiatan berorganisasi ataupun melakukan kegiatan yang tujuannya untuk mencapai tujuan dari masing-masing individu itu tersendiri.

Kampus merupakan Miniatur Negara. “Kampus merupakan miniatur Negara, di dalamnya banyak berbagai jenis orang, kebudayaan dan Organisasi, di dalamnya terdapat mahasiswa-mahasiswa yang bertanggungjawab atas dirinya,”

...

,okok selanjutnya itu topologi kampus
Tipologi mahasiswa merupakan pengelompokkan watak, sifat mahasiswa berdasarkan dengan apa yang mereka minati.
Dalam pengklasifikasian ini sifatnya tidak bisa dibilang paten, karena setiap diri kita bisa membuat tipologi sesuai dengan yang kita lihat dan rasakan. Yang paling penting dari pengklasifikasian mahasiswa ini adalah, kita mampu memetakan jenis-jenis mahasiswa sehingga mampu “bermain” dalam lingkungan tersebut.
Kita mencoba melakukan klasifikasi atas tipologi mahasiswa, walau ini tidak bersifat paten karena setiap diri kita bisa membuat tipologi sesuai dengan yang kita lihat dan rasakan. Anda sendiri bisa memegang dua katagori atau tiga bahkan empat sekaligus dari tipologi yang kitra susun ini. Bahkan mungkin masih membuka munculnya jenis tipologi lainnya. Yang penting semoga Anda bisa berguna bagi diri Anda sendiri dan bagi orang lain dalam lingkungan kehidupan keluarga, organisasi dan masyarakat.


- Study Oriented
Tipikal mahasiswa jenis ini selalu rajin masuk kuliah dan melaksanakan tugas-tugas akademik. Mahasiswa jenis ini tidak mau tahu dengan apa yang terjadi di kampus. Pokoknya yang penting mendapatkan nilai bagus dan cepat lulus.
- Hedonis
Ia selalu menjalani kehidupan dengan hedonis, glamour, dan happy-happy.
- Agamis
kemana-mana selalu membawa al-Qur’an, berpakaian ala orang Arab, tampil (sok) islami, menjaga jarak terhadap lain jenis yang tidak muhrim.
- K3
kesibukanya hanya K3, yaitu kampus, kos dan kampung. Kalau tiba jam kuliah ya berangkat kuliah, kalau selesai pulang kos, atau ada waktu cukup pulang kampung.
- Santai semaunya sendiri
Tiada banyak berpikir, selalu menjalani kehidupan apa adanya. Enjoy aja! Dia tidak terlalu memikirkan kuliah, karena yang penting dalam hidupnya adalah santai. Biasanya mahasiswa seperti ini lama sekali lulusnya, karena nilainya juga santai.
- Mencari cinta
tiada terlalu memikirkan kuliah, tetapi yang dipikirkannya adalah CINTA. Yang penting baginya adalah mendapatkan pacar yang setia. Lulus kuliah cepet-cepet menikah.
- Anak mami
mungkin pulang di akhir pekan, takut kalau mamanya marah. Ia kuliah demi menyenangkan hati maminya. Kebanyakan tipikal seperti ini tidak menikmati perkuliahannya, karena jurusan perkuliahannya itu pilihan dari sang ibunda, bukan dari kehendak hatinya. Kebanyakan tipe kuliah seperti ini putus di tengah jalan, tetapi semoga kamu tidak!
- Mahasiswa apa mahasiswi
memiliki dua kepribadian, yang pertama wanita yang kedua pria. Orang-orang biasa menyebutnya banci, tidak punya karakter yang jelas.
- Monitor
selalu berhadapan dengan komputer, sampai-sampai mukanya sudah berevolusi seperti monitor.
- Pemikir
selalu berpikir dan terus berpikir. Hobinya membaca buku, diskusi dan menulis. Terkadang orang jenis ini –karena terus belajar- tanpa menghiraukan sekitarnya, agar bisa mendapatkan jawaban atas apa yang dipikirkannya. Biasanya tipe mahasiswa seperti ini jika telah lulus ingin jadi ilmuwan, peneliti, dosen atau akademisi.
- Pemimpin
selalu terlihat mencolok dan aktif dibandingkan mahasiswa lainnya. Hidupnya di perkuliahan sangat bervariatif –diisi dengan berbagai kegiatan, dan ia tidak hanya belajar dari kuliah semata, namun juga belajar dari lingkungan. Ia akan aktifg di organisasi, baik intra maupun ektra kampus. Biasanya –tapi tidak mengikat- tipe mahasiswa seperti ini tidak memiliki keinginan yang besar untuk lulus terlalu cepat, karena ia mencari pengalaman sebanyak-banyaknya untuk menjadi pemimpin di masa depan. Cita-citanya, biasanya ingin menjadi pemimpin perusahaan, lurah, bupati, DPR, menteri, bahkan presiden.
- Abadi
Jelas, mahasiswa jenis ini paling betah di kampus, yang di kuliahnya di atas semester 10 tapi masih santai-santai dan belum mikir lulus.
- Gajelas
Seperti ini tak bisa dikategorikan, karena terkadang ia seperti pemimpin, terkadang seniman, terkadang pemikir, terkadang santai, terkadang pecinta, terkadang usil, dll. Terkadang aktif keliatan terus, terkadang lenyap hilang entah ke mana.

Secara material/fisik, pembaruan atau inovasi yang dimaksu adalah penemuan (discovery) sebagai dampak dari berkembangnya pola pikir manusia dan adanya bantuan teknologi. Namun secara sosial, adanya pembaruan terjadi karena adanya evaluasi atas kehidupan, dimana timbul pengetahuan/pemikiran baru. Dalam dunia kemahasiswaan hal ini bisa berupa berubahnya arah atau tujuan

mahasiswa menganggap dirinya berperan sebagai pelopor perubahan bangsa namun pelan-pelan pemahaman tersebut dialihkan bahwa mahasiswa berperan sebagai seseorang yang harus belajar dan mendapatkan prestasi.

"Seorang terpelajar harus bersikap adil sejak dalam pikiran, apalagi perbuatan" - Pramoedya Ananta Toer
