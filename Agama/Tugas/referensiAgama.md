AKURAT.CO, Setiap anak yang terlahir di dunia ini pada dasarnya adalah suci, barulah orang tua yang bertanggung jawab terhadap tumbuh kembangnya bahkan terkait dengan keimanan seorang anak.

Di dalam ajaran Islam, ketika bayi baru dilahirkan, setiap orang tua dianjurkan untuk azan di telinga kanan bayi dan iqamah di telinga kirinya. Mungkin sebagian dari kita masih bertanya-tanya untuk apa bayi diazankan? Apakah mereka langsung disuruh salat?

Azan seperti yang kita dengar sehari-hari bukan hanya berfungsi sebagai penanda waktu salat. Namun azan juga berguna untuk mengiringi berbagai macam hal, salah satunya ketika bayi pertama kali melihat dunia.

Dalam sebuah hadis dari Ubaidillah bin Abi Rafi' ra, dari ayahnya ia berkata, "Aku melihat Rasulullah saw mengumandangkan azan di telinga Husain bin Ali ketika Siti Fatimah melahirkannya (yakni) dengan azan salat." (HR. Abu Dawud)

Meski menurut At-Tirmidzi dalam karyanya yang berjudul Tuhfatul Ahwadzi hadis di atas tergolong lemah, tetapi kita boleh mengamalkannya karena hadis di atas dikuatkan dengan hadis lainnya.

Dari Husain bin Ali ia pernah mendengar Rasulullah Saw bersabda, "Orang yang anaknya baru lahir, maka azankanlah pada telinga kanannya, dan bacakanlah iqamah pada telinga kirinya. Dijamin anak tersebut tidak akan diganggu setan."

Sementara itu dalam Majmu' Fatawi wa Rasail diterangkan, "Yang pertama mengumandangkan azan di telinga kanan anak yang baru lahir, selanjutnya membacakan iqamah di telinga kiri. Para ulama telah menetapkan bahwa perbuatan ini tergolong sunnah. Mereka telah mengamalkan hal tersebut tanpa seorang pun yang mengingkarinya. Perbuatan ini ada relevansi yakni untuk mengusir setan dari anak yang baru lahir tersebut. Karena setan akan lari terbirit-birit ketika mereka mendengar azan sebagaimana ada keterangan di dalam berbagai hadis."

Jadi, dapat disimpulkan bahwa azan dan iqamah bukan hanya panggilan untuk salat melainkan juga untuk mengusir setan yang selalu mengganggu manusia sampai kiamat kelak.




Anak adalah titipan Ilahi. Anak merupakan amanah yang harus dijaga dengan baik. Dalam upaya itulah seringkali orang tua berusaha sedemikian rupa agar kelak anak-anaknya menjadi orang yang shaleh/sholehah berguna bagi masyarakat dan agama. Dalam hal kesehatan jasmani, semenjak dalam kandungan oang tua telah berusaha menjaga kesehatannya dengan berbagai macam gizi yang dimakan oleh sang ibu. Begitu juga kesehatan mentalnya. Semenjak dalam kandungan orang tua selalu rajin berdoa dan melakukan bentuk ibadah tertentu dengan harapan amal ibadah tersebut mampu menjadi wasilah kesuksesan calon si bayi. <>Oleh karena itu ketika dalam keadaan mengandung pasangan orang tua seringkali melakukan riyadhoh untuk sang bayi. Misalkan puasa senin-kamis atau membaca surat-surat tertentu seperti Surat Yusuf, Surat maryam, Waqiah, al-Muluk dan lain sebagainya. Semuanya dilakukan dengan tujuan tabarrukan dan berdoa semoga si bayi menjadi seperti Nabi Yusuf bila lahir lelaki. Atau seperti Siti Maryam bila perempuan dengan rizki yang melimpah dan dihormati orang.

Begitu pula ketika sang bayi telah lahir di dunia, do’a sang Ibu/Bapak tidak pernah reda. Ketika bayi pertama kali terdengar tangisnya, saat itulah sang ayah akan membacakannya kalimat adzan di telinga sebelah kanan, dan kalimat iqamat pada telinga sebelah kiri. Tentunya semua dilakukan dengan tujuan tertentu. Lantas bagaimanakah sebenarnya Islam memandang hal-hal seperti ini? Bagaimanakah hukum mengumandangkan adzan dan iqamah pada telinga bayi yang baru lahir? berdasarkan sebuah hadits dalam sunan Abu Dawud (444) ulama bersepakatn menghukumi hal tersebut dengan sunnah : عن عبيد الله بن أبى رافع عن أبيه قال رأيت رسول الله صلى الله عليه وسلم أذن فى أذن الحسن بن علي حين ولدته فاطمة بالصلاة (سنن أبي داود رقم 444) Dari Ubaidillah bin Abi Rafi’ r.a Dari ayahnya, ia berkata: aku melihat Rasulullah saw mengumandangkan adzan di telinga Husain bin Ali ketika Siti Fatimah melahirkannya (yakni) dengan adzan shalat. (Sunan Abu Dawud: 444) Begitu pula keterangan yang terdapat dalam Majmu’ fatawi wa Rasail halaman 112. Di sana diterangkan bahwa: “yang pertama mengumandangkan adzan di telinga kanan anak yang baru lahir, lalu membacakan iqamah di telinga kiri. Ulama telah menetapkan bahwa perbuatan ini tergolong sunnah. Mereka telah mengamalkan hal tersebut tanpa seorangpun mengingkarinya. Perbiatan ini ada relevansi, untuk mengusir syaithan dari anak yang baru lahir tersebut. Karena syaitan akan lari terbirit-birit ketika mereka mendengar adzan sebagaimana ada keterangan di dalam hadits. (Sumber; Fiqih Galak Gampil 2010)



















Azan dan iqamah termasuk syiar umat Islam dan disunnahkan mengumandangkannya ketika masuk waktu shalat. Selain itu, ulama juga menganjurkan mengumandangkan adzan beserta iqamah pada saat melakukan perjalanan. Disunnahkan pula mengumandangkan adzan dan iqamah saat anak dilahirkan.   Tidak hanya itu, mayoritas masyarakat Indonesia juga membudayakan mengazankan mayat ketika hendak dikubur. Setelah mayat diletakkan di liang lahat, kain kafan dibuka, dan muka mayat ditempelkan ke tanah sembari menghadap kiblat, salah satu dari orang yang menguburkan mengumandangkan adzan sebagai bentuk penghormatan terakhir.

Ada ulama yang mengatakan, adzan dan iqamah disunnahkan ketika menguburkan mayat. Kesunnahan ini disamakan (qiyas) dengan kesunnahan mengazankan anak yang baru lahir. Akan tetapi, menyamakan hukum mengazankan mayat dengan bayi yang baru lahir ini dianggap lemah oleh ulama lain. Syekh Ibrahim al-Baijuri dalam Hasyiyah al-Baijuri menjelaskan:   ويسن الأذان والإقامة أيضا خلف المسافر ولا يسن الأذان عند إنزال الميت القبر خلافا لمن قال بسنيته قياسا لخروجه من الدنيا على دخوله فيها قال ابن حجر ورددته في شرح العباب لكن إن وافق إنزاله القبر بأذان خفف عنه في السؤال   “Disunnahkan adzan dan iqamah saat melakukan perjalanan dan tidak disunnahkan adzan ketika menguburkan mayat. Pendapat ini berbeda dengan ulama yang mensunnahkan adzan karena menyamakan hukumnya dengan mengazankan  anak yang baru lahir. Ibnu Hajar berkata, saya menolaknya dalam Syarah al-‘Ubab, akan tetapi jika penguburan mayat disertai adzan, maka mayat diringankan dalam menjawab pertanyaan di dalam kubur”

Dari penjelasan di atas, dapat dipahami bahwa ulama berbeda pendapat tentang hukum adzan dan iqamah ketika menguburkan mayat. Ada yang mengatakan sunnah dan ada yang tidak. Perbedaan ini didasarkan pada perbedaan mereka dalam memahami hadis Nabi. Ulama yang mengatakan tidak sunnah beragumentasi dengan tidak adanya dalil spesifik dan pasti terkait permasalahan ini. Sementara ulama yang membolehkannya menganalogikan kasus ini dengan kesunnahan mengazankan anak yang baru lahir. Kendati tidak ada dalil spesifik, namun perlu diingat bahwa adzan dan iqamah termasuk bagian dari dzikir. Sebagaimana diketahui, zikir disunnahkan melafalkannya kapan pun dan di mana pun kecuali di tempat-tempat yang dilarang, seperti saat buang hajat. Oleh sebab itu, mengazankan mayat dibolehkan karena bagian dari zikir. Hikmahnya, sebagaimana dikutip al-Baijuri di atas, membantu mayat dan meringankannya dalam menjawab pertanyaan malaikat di dalam kubur. Wallahu a’lam. (Hengki Ferdiansyah)



BincangSyariah.Com – Di kalangan masyarakat muslim Indonesia, adzan dan iqamah tidak hanya dikumandangkan untuk menandai masuknya waktu shalat wajib. Namun banyak hal-hal tertentu yang juga didahului adzan dan iqamah, di antaranya adalah ketika hendak menguburkan jenazah. Sebenarnya, bagaimana hukum mengumandangkan adzan dan iqamah ketika hendak menguburkan jenazah? (Baca: Anjuran Adzan dan Iqamat pada Telinga Bayi yang Baru Lahir)

Dalam kitab Al-Mausu’ah Al-Fiqhiyah Al-Kuwaitiyah disebutkan bahwa mengumandangkan adzan dan iqamah pada saat hendak menguburkan jenazah adalah sunnah. Menurut ulama Syafiiyah, adzan dan iqamah tidak hanya dianjurkan ketika masuk waktu shalat atau hendak melaksanakan shalat saja, namun juga dianjurkan dalam hal-hal lainnya seperti hendak menguburkan jenazah, ketika terjadi kebakaran dan lainnya.

Bahkan menurut ulama Syafiiyah, selain dikumandangkan untuk menandai masuknya waktu shalat wajib, ada dua belas perkara yang disunnahkan untuk mengumandangkan adzan dan iqamah. Yaitu, pada telinga bayi baru lahir, pada telinga orang yang sedang kesusahan, di belakang orang hendak bepergian, ketika terjadi kebakaran, ketika terjadi serdadu sedang berkecamuk dalam perang, ketika ada gangguan jin, ketika tersesat dalam perjalanan, pada telinga orang yang kesurupan, ketika marah, pada telinga orang atau hewan yang sangat nakal, juga ketika hendak menguburkan jenazah.

Ini sebagaimana disebutkan dalam kitab Al-Mausu’ah Al-Fiqhiyah Al-Kuwaitiyah berikut;

شرع الأذان أصلا للإعلام بالصلاة إلا أنه قد يسن الأذان لغير الصلاة تبركا واستئناسا أو إزالة لهم طارئ والذين توسعوا في ذكر ذلك هم فقهاء الشافعية فقالوا : يسن الأذان في أذن المولود حين يولد ، وفي أذن المهموم فإنه يزيل الهم ، وخلف المسافر ، ووقت الحريق ، وعند مزدحم الجيش ، وعند تغول الغيلان وعند الضلال في السفر ، وللمصروع ، والغضبان ، ومن ساء خلقه من إنسان أو بهيمة ، وعند إنزال الميت القبر قياسا على أول خروجه إلى الدنيا

Pada dasarnya, adzan disyariatkan untuk memberitahu masuknya waktu shalat, akan tetapi terkadang adzan disunnahkan untuk selain shalat karena untuk mengambil berkah dan untuk menghilangkan kesusahan yang datang. Ulama yang banyak membahas hal itu adalah ulama Syafiiyah. Mereka berkata, ‘Adzan disunnahkan untuk dikumandangkan pada telinga bayi yang baru lahir, pada telinga orang kesusahan agar kesusahannya hilang, di belakang orang yang bepergian, ketika terjadi kebakaran, ketika serdadu berdesak-desakan, ketika ada gangguan jin, ketika tersesat dalam perjalanan, pada orang yang kesurupan, ketika marah, pada orang atau hewan yang jelek perangainya, dan ketika menurunkan jenazah ke dalam kubur karena disamakan dengan pertama kalinya keluar ke alam dunia.


