List of Keyboard Shortcuts for Spreadsheets
Shortcut keys: Function
Alt+Enter - Starts a new line in the same cell.
Alt+F11 - Displays the Microsoft Visual Basic Editor
Alt+F8 - Displays the Macros dialog box to run, edit or delete macros.
Alt+Space - Displays the Control menu for the program window
ARROW KEYS - Move one cell up, down, left, or right in a worksheet.
Backspace - Clears the content of the active cell to reedit cell content.
Ctrl+' - Enters the current time.
Ctrl+; - Enters the current date.
Ctrl+1 - Displays the Format Cells dialog box.
Ctrl+Arrow keys - Selects the entire worksheet.
Ctrl+Arrow keys - Moves to the edge of the current data region in a worksheet.
Ctrl+B - Applies or removes bold formatting.
Ctrl+C - Copies the selected cells.
Ctrl+D - Uses the Fill Down command to copy the contents and format of the topmost cell of a selected range into the cells below.
Ctrl＋End - CTRL+END moves to the last cell on a worksheet, in the lowest used row of the rightmost used column.
Ctrl+Enter - Fills the selected cell range with the current entry.
Ctrl+F - Displays the Find dialog box.
Ctrl+F1 - Displays the Task Pane on the right side of the program window.
Ctrl+G - Displays the Go To dialog box.
Ctrl+H - Displays the Replace dialog box.
Ctrl+Home - Moves to the beginning of a worksheet.
Ctrl+I - Applies or removes italic formatting.
Ctrl+K - Displays the Hyperlink dialog box to insert new hyperlinks or edit existing hyperlinks.
Ctrl+mouse click - Selects multiple ranges of cells.
Ctrl+N - Creates a new, blank workbook.
Ctrl+O - Displays the Open dialog box to open or find a file.
Ctrl+P - Displays the Print dialog box.
Ctrl+PageDown - Switches between worksheet tabs, from right-to-left.
Ctrl+PageUp - Switches between worksheet tabs, from left-to-right.
Ctrl+R - Uses the Fill Right command to copy the contents and format of the leftmost cell of a selected range into the cells to the right.
Ctrl+S - Saves the active file with its current file name, location, and file format.
Ctrl+Shift+' - Enters the current time.
Ctrl+Shift+; - Enters the current time.
Ctrl+Shift+= - Displays the Insert dialog box.
Ctrl+Shift+ARROW - Extends the selection of cells to the last nonblank cell in the same column or row as the active cell, or if the next cell is blank, extends the selection to the next nonblank cell.
Ctrl+Shift+Home - Extends the selection of cells to the beginning of the worksheet.
Ctrl+Shift+Tab - Goes to previous worksheet tab.
Ctrl+Tab - Goes to next worksheet tab.
Ctrl+U - Applies or removes underlining.
Ctrl+V - Inserts the contents of the Clipboard at the insertion point and replaces any selection.
Ctrl+W - Closes the selected workbook window.
Ctrl+X - Cuts the selected cells.
Ctrl+Y - Repeats the last command or action, if possible.
Ctrl+Z - Uses the Undo command to reverse the last command or to delete the last entry you typed.
Enter - Moves one cell down in a selected cell range.
F1 - Help
F2 - Edits the active cell and positions the insertion point at the end of the cell contents.
F4 - Repeats the last action of adding or deleting the whole row/column.
F7 - Displays the Spelling dialog box to check spelling in the active worksheet or selected range.
F9 - Calculates all worksheets in all open workbooks.
F11 - Insert chart.
F12 - Displays the Save As dialog box to save the file with a different name, type or location.
Home - Moves to the cell in the upper-left corner of the window.
Mouse pointer - Selects range of cells.
PageDown - Selects the cells in the next screen in the same column(s).
PageUp - Selects the cells in the previous screen in the same column(s).
Shift+ARROW - Extends the selection of cells by one cell, or row/column in that direction.
Shift+Enter - Moves one cell up in a selected cell range.
Shift+f10 - Displays context menu for a selected item.
Shift+Home - Extends the selection of cells to the first row of this window in this column.
Shift+Insert - Inserts the contents of the Clipboard at the insertion point and replaces any selection.
Shift+mouse - Selects the area from the active cell to the clicked cell.
Shift+PageDown - Selects the cell range from the current cells to the next screen of the same column(s).
Shift+PageUp - Selects the cell range from the current cells to the previous screen of the same column(s).
Shift+Tab - Moves to the previous cell in a worksheet, or moves one cell to the left in a selected range.
Tab - Moves one cell to the right in a worksheet.
