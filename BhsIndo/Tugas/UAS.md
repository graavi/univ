Buatlah Makalah maksimal 20 halaman (kaver, kata pengantar dan daftar isi tidak dihitung) dengan ketentuan: 

1.     Topik bebas. 

2.     Ditulis dengan huruf times new roman 12pt. Spasi 1,5. 

3.     Ukuran kertas A4. 

4.     Menggunakan sistem rujukan APA (American Pshycological Association) dengan modifikasi sesuai aturan dari pedoman penulisan karya Ilmiah Unusia [lihat buku pedoman oenulisan karya ilmiah Unusia]. 

5.     Makalah berisi 15 referensi (Buku atau artikel ilmiah) 

6.     Makalah berisi 15 kutipan yang terdiri dari 3 kutipan langsung panjang, 7 kutipan langsung pendek dan 5 kutipan tidak langsung. 

7.     Makalah harus ditulis dengan susunan sebagaimana berikut: 

1.    Cover

2.    Kata Pengantar

3.    Daftar Isi

4.    BAB I

PENDAHULUAN

·      Latar Belakang Masalah

·      Rumusan Masalah

·      Tujuan Pembahasan

5.    BAB II

PEMBAHASAN

6.    BAB III

KESIMPULAN

7.    DAFTAR PUSTAKA

 

Makalah harus diunggah ke e-campus dalam format Pdf (Format selain PDF tidak diterima).
